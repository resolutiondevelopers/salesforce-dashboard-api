using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Hangfire;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using SalesForceReportSVC.Controllers;
using SalesForceReportSVC.DTOs;

namespace SalesForceReportSVC
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly SalesForceDataLoaderController _medAPIClient;
        private IConfiguration Configuration;
        private string HangfireDb = String.Empty;

        public Worker(ILogger<Worker> logger, SalesForceDataLoaderController medAPIClient, IConfiguration configuration)
        {
            _logger = logger;
            //_salesforceAPIClient = salesforceAPIClient;
            _medAPIClient = medAPIClient;
            this.Configuration = configuration;

            this.HangfireDb = this.Configuration.GetValue<string>("DefaultSetting:hangfiredb");
            //GlobalConfiguration.Configuration.UseSqlServerStorage(this.HangfireDb);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);

                //_medAPIClient.GetSDL().GetAwaiter();
                //_medAPIClient.GetSDL().GetAwaiter()

                RecurringJob.AddOrUpdate(
                            () => _medAPIClient.GetSDL().GetAwaiter(),
                            Cron.Daily(7,0));

                await Task.Delay(10000, stoppingToken);

                
            }
           
        }

    }
}
