﻿using Microsoft.EntityFrameworkCore;
using SalesForceReportSVC.DTOs;
using System;

namespace SalesForceReportSVC.DAL
{
    public class SalesForceReportLoaderContext : DbContext
    {

        public SalesForceReportLoaderContext(DbContextOptions<SalesForceReportLoaderContext> options)
            : base(options)
        {

        }

        public virtual DbSet<SalesForceProductionReportDto> productionReports { get; set; }

        public virtual DbSet<ActWhatsAppClaimStatusDto> actWhatsAppClaimStatuses { get; set; }

        public virtual DbSet<PureWhatsAppClaimStatusDto> pureWhatsAppClaimStatus { get; set; }
        public virtual DbSet<PureWhatsAppPolicyDetailsDto> pureWhatsAppPolicyDetails { get; set; }
        public virtual DbSet<WhatsAppUpdateContactDto> updateContactDtos { get; set; }
        public virtual DbSet<WhatsAppSTKPushRequest> sTKPushRequests { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SalesForceProductionReportDto>().HasNoKey().ToView("DailyProductionReport_Vw");
            modelBuilder.Entity<ActWhatsAppClaimStatusDto>().HasNoKey().ToView("Act_Watsup_Claims");
            modelBuilder.Entity<PureWhatsAppClaimStatusDto>().HasNoKey().ToView("vw_Pure_WhatsApp_Claim_Status");
            modelBuilder.Entity<PureWhatsAppPolicyDetailsDto>().HasNoKey().ToView("vw_Pure_WhatsApp_Policy_Details");
            modelBuilder.Entity<WhatsAppStatementRequestDto>().ToTable("tbl_Statement_Request").HasKey(x => x.StId);
            modelBuilder.Entity<WhatsAppSTKPushRequest>().ToTable("tbl_WhatsApp_STK_Requests").HasKey(x => x.Id);
            modelBuilder.Entity<WhatsAppUpdateContactDto>().ToTable("tbl_WhatsApp_Update_Contacts").HasKey(x => x.ID);


            modelBuilder.Entity<WhatsAppStatementRequestDto>().Property<int>(x => x.StId).UseIdentityColumn<int>().ValueGeneratedOnAdd();
            modelBuilder.Entity<WhatsAppSTKPushRequest>().Property<int>(x => x.Id).UseIdentityColumn<int>().ValueGeneratedOnAdd();
            modelBuilder.Entity<WhatsAppUpdateContactDto>().Property<Int32>(x => x.ID).UseIdentityColumn<Int32>().ValueGeneratedOnAdd();
            base.OnModelCreating(modelBuilder);

        }
    }

    
}
