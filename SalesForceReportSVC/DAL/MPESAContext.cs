﻿using Microsoft.EntityFrameworkCore;
using SalesForceReportSVC.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalesForceReportSVC.DAL
{
    public class MPESAContext : DbContext
    {
        public MPESAContext(DbContextOptions<MPESAContext> options)
            : base(options)
        {

        }
        public virtual DbSet<MPESAConfirmationDto> MPESAConfirmations { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MPESAConfirmationDto>().ToTable("c2b2_callbacks").HasKey(x => x._id);
            base.OnModelCreating(modelBuilder);
        }

    }
}
