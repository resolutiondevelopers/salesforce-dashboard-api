﻿using AutoMapper;
using SalesForceReportSVC.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SalesForceReportSVC.Services
{
    public class AutoMapping: Profile
    {
        public AutoMapping()
        {
            CreateMap(typeof(IQueryable<>), typeof(IEnumerable<>));
            CreateMap<ActWhatsAppStatementReq, WhatsAppStatementRequestDto>()
                .ForMember(dest => dest.BArea, opt => opt.MapFrom(src => 1))
                .ForMember(dest => dest.dateRequested, opt => opt.MapFrom(src => DateTime.Now))
                .ForMember(dest => dest.status, opt => opt.MapFrom(src => 1));


            CreateMap<PureWhatsAppStatementReq, WhatsAppStatementRequestDto>()
               .ForMember(dest => dest.BArea, opt => opt.MapFrom(src => 2))
               .ForMember(dest => dest.dateRequested, opt => opt.MapFrom(src => DateTime.Now))
               .ForMember(dest => dest.status, opt => opt.MapFrom(src => 1));
        }
    }
}
