﻿using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SalesForceReportSVC.Controllers;
using SalesForceReportSVC.DAL;
using SalesForceReportSVC.DTOs;
using SalesForceReportSVC.Helpers;
using SalesForceReportSVC.Repositories;
using System;
using Hangfire;
//using Hangfire.SqlServer;
using System.Collections.Generic;
using System.Text;

namespace SalesForceReportSVC
{
    public class Startup
    {
        private IConfiguration Configuration { get; }
        private string APIdb = String.Empty;
        private string MPESAdb = String.Empty;
        private string HangfireDb = String.Empty;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            this.APIdb = /*"Data Source=192.168.0.58,1433;Initial Catalog=API_Integrations;user id=Actisure_User; password=Cegedim2018!;Integrated Security=false; Connect Timeout=9999999";*/this.Configuration.GetValue<string>("DefaultSetting:APIdb").ToString();
            this.MPESAdb = /*"server=192.168.0.140;port=3306;database=oapi;uid=root;password=4112";*/this.Configuration.GetValue<string>("DefaultSetting:MPESADb").ToString();
            this.HangfireDb = this.Configuration.GetValue<string>("DefaultSetting:hangfiredb");


            MemoryStorageHelper.BaseEndpoint = new Uri(this.Configuration.GetValue<string>("DefaultSetting:SalesforceEndpoint").ToString());
            MemoryStorageHelper.ClientId = this.Configuration.GetValue<string>("DefaultSetting:clientId").ToString();
            MemoryStorageHelper.ClientSecret = this.Configuration.GetValue<string>("DefaultSetting:clientSecret").ToString();
            MemoryStorageHelper.Username = this.Configuration.GetValue<string>("DefaultSetting:username").ToString();
            MemoryStorageHelper.Password = this.Configuration.GetValue<string>("DefaultSetting:pass").ToString();
        }

        public void ConfigureServices(Microsoft.Extensions.DependencyInjection.IServiceCollection services)
        {
            services.AddHostedService<Worker>();

            ////Add Controllers
            //services.AddTransient<SalesForceDataLoaderController>();


            //Add DB Dependency Injection
            services.AddAutoMapper(typeof(Startup));
            services.AddControllers().AddNewtonsoftJson();

            // Controllers
            //services.AddTransient<ActisureWhatsAppController>();

            services.AddScoped<IRepositoryWrapper, RepositoryWrapper>();
            //services.AddScoped<ILogger, Logger<SalesForceDataLoaderController>>();
            //services.AddScoped<ILogger, Logger<ActisureWhatsAppController>>();
            //services.AddScoped<ILogger, Logger<PureWhatsAppController>>();
            services.AddScoped<ILogger, Logger<ActisureWhatsAppController>>();

            services.AddSingleton(typeof(IWhatsAppRepository<>), typeof(WhatsAppRepository<>));
            services.AddSingleton<ISalesForceDashboardReportRepository, SalesForceDashboardReportRepository>();
            services.AddSingleton<IWhatsAppActClaimStatusRepository, WhatsAppActClaimStatusRepository>();
            services.AddSingleton<IWhatsAppPureClaimStatusRepository, WhatsAppPureClaimStatusRepository>();
            services.AddSingleton<IWhatsAppPurePolicyDetailsRepository, WhatsAppPurePolicyDetailsRepository>();

            services.AddSingleton<IRepositoryWrapper, RepositoryWrapper>();
            //services.AddSingleton<IMPESAConfirmationRepo, MPESAConfirmationRepo>();


            //services.AddSingleton<SalesforceAPIClient>();

            services.AddDbContext<MPESAContext>(ConfigureMPESADb);
            services.AddDbContext<SalesForceReportLoaderContext>(ConfigureAPIIntegrations);

            services.AddSingleton<ISalesforceMedAPIClient, SalesforceMedAPIClient>();
            services.AddSingleton<SalesForceDataLoaderController>();


            //services.AddHangfire(config =>
            //{
            //    config.UseSqlServerStorage(this.HangfireDb);
            //});

            services.AddHangfire(config =>
            {
                var options = new Hangfire.SqlServer.SqlServerStorageOptions
                {
                    PrepareSchemaIfNecessary = true,
                    QueuePollInterval = TimeSpan.FromMinutes(5)
                };
                config.UseSqlServerStorage(this.HangfireDb, options);
            });
        }


        /// <summary>
        /// Add Configure Databases
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        private void ConfigureMPESADb(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySql(this.MPESAdb);
            optionsBuilder.EnableDetailedErrors();
        }

        private void ConfigureAPIIntegrations(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(this.APIdb);
            optionsBuilder.EnableDetailedErrors();
        }


        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            //GlobalConfiguration.Configuration.UseSqlServerStorage(this.HangfireDb);
            ////app.UseHangfireServer();
            //app.UseHangfireDashboard("/hangfire", new DashboardOptions
            //{
            //    Authorization = new[] {new HangfireDashboardAuthorizationFilter()}
            ////});
            ///Persist Security Info=True;

            app.UseHangfireServer(new BackgroundJobServerOptions
            {
                WorkerCount = 1
            });
            app.UseHangfireDashboard();

            GlobalJobFilters.Filters.Add(new AutomaticRetryAttribute { Attempts = 0 });
            
        }

    }
}
