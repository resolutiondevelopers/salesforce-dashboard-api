﻿using SalesForceReportSVC.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalesForceReportSVC.Repositories
{
    public interface IWhatsAppPurePolicyDetailsRepository: IWhatsAppRepository<PureWhatsAppPolicyDetailsDto>
    {
    }
}
