﻿using SalesForceReportSVC.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalesForceReportSVC.Repositories
{
    public class WhatsAppStatementRequestRepository: WhatsAppRepository<WhatsAppStatementRequestDto>, IWhatsAppStatementRequestRepository
    {
        public WhatsAppStatementRequestRepository(SalesForceReportSVC.DAL.SalesForceReportLoaderContext loaderContext)
            :base(loaderContext)
        {

        }
    }
}
