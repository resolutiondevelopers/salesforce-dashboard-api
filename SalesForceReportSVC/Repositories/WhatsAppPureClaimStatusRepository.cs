﻿using SalesForceReportSVC.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalesForceReportSVC.Repositories
{
    public class WhatsAppPureClaimStatusRepository:WhatsAppRepository<PureWhatsAppClaimStatusDto>, IWhatsAppPureClaimStatusRepository
    {
        public WhatsAppPureClaimStatusRepository(SalesForceReportSVC.DAL.SalesForceReportLoaderContext loaderContext)
            :base(loaderContext)
        {

        }
    }
}
