﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesForceReportSVC.Repositories
{
    public interface IRepositoryWrapper
    {
        void Save();
        IMPESARepository MPESARepository { get; }
        IWhatsAppActClaimStatusRepository whatsAppActClaim { get; }
        IWhatsAppPureClaimStatusRepository whatsAppPureClaim { get; }
        IWhatsAppStatementRequestRepository statementRequestRepository { get; }
        IWhatsAppPurePolicyDetailsRepository WhatsAppPurePolDetails { get; }
        ISalesForceDashboardReportRepository reportRepository { get; }
        IWhatsAppUpdateContactsRepository whatsAppUpdateContacts { get; }
        IWhatsAppSTKPushRepository whatsAppSTK { get; }
        ISalesForceDashboardReportRepository salesforceLoader { get; }
    }
}
