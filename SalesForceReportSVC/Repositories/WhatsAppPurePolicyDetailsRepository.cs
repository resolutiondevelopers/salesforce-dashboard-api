﻿using SalesForceReportSVC.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalesForceReportSVC.Repositories
{
    public class WhatsAppPurePolicyDetailsRepository: WhatsAppRepository<PureWhatsAppPolicyDetailsDto>, IWhatsAppPurePolicyDetailsRepository
    {
        public WhatsAppPurePolicyDetailsRepository(SalesForceReportSVC.DAL.SalesForceReportLoaderContext loaderContext)
            :base(loaderContext)
        {

        }
    }
}
