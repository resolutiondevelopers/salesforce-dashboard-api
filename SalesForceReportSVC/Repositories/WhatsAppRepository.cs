﻿using Microsoft.EntityFrameworkCore;
using SalesForceReportSVC.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace SalesForceReportSVC.Repositories
{
    public class WhatsAppRepository<T> : IWhatsAppRepository<T> where T:class
    {
        protected SalesForceReportLoaderContext _Context { get; set; }

        public WhatsAppRepository(SalesForceReportLoaderContext context)
        {
            this._Context = context;
        }

        public IQueryable<T> FindAll()
        {
            return this._Context.Set<T>().AsNoTracking();
        }

        public IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression)
        {
            return this._Context.Set<T>().Where(expression).AsNoTracking();
        }

        public void Create(T entity)
        {
            this._Context.Set<T>().Add(entity);
        }
    }
}
