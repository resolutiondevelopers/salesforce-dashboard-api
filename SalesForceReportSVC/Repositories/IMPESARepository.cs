﻿using SalesForceReportSVC.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace SalesForceReportSVC.Repositories
{
    public interface IMPESARepository
    {
        IQueryable<MPESAConfirmationDto> FindMPESAByCondition(Expression<Func<MPESAConfirmationDto, bool>> expression);
    }
}
