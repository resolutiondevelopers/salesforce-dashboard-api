﻿using SalesForceReportSVC.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalesForceReportSVC.Repositories
{
    public class WhatsAppActClaimStatusRepository: WhatsAppRepository<ActWhatsAppClaimStatusDto>, IWhatsAppActClaimStatusRepository
    {

        public WhatsAppActClaimStatusRepository(SalesForceReportSVC.DAL.SalesForceReportLoaderContext loaderContext)
            : base(loaderContext)
        {

        }
    }
}
