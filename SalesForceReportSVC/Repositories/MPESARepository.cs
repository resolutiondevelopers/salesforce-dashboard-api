﻿using Microsoft.EntityFrameworkCore;
using SalesForceReportSVC.DAL;
using SalesForceReportSVC.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace SalesForceReportSVC.Repositories
{
    public class MPESARepository : IMPESARepository
    {
        protected MPESAContext mpesaContext { get; set; }

        public MPESARepository(MPESAContext context)
        {
            this.mpesaContext = context;
        }

        public IQueryable<MPESAConfirmationDto> FindMPESAByCondition(Expression<Func<MPESAConfirmationDto, bool>> expression)
        {
            return this.mpesaContext.Set<MPESAConfirmationDto>().Where(expression).AsNoTracking();
        }

    }
}
