﻿using SalesForceReportSVC.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SalesForceReportSVC.Repositories
{
    public interface ISalesForceDashboardReportRepository
    {
        IQueryable<SalesForceProductionReportDto> FindAll();
        
    }
}
