﻿using Microsoft.EntityFrameworkCore;
using SalesForceReportSVC.DAL;
using SalesForceReportSVC.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SalesForceReportSVC.Repositories
{
    public class SalesForceDashboardReportRepository : ISalesForceDashboardReportRepository
    {
        private readonly SalesForceReportLoaderContext salesForceReport;

        public SalesForceDashboardReportRepository(SalesForceReportLoaderContext loaderContext)
        {
            this.salesForceReport = loaderContext;
        }

        public IQueryable<SalesForceProductionReportDto> FindAll()
        {
            return this.salesForceReport.Set<SalesForceProductionReportDto>().AsNoTracking();
        }
    }
}
