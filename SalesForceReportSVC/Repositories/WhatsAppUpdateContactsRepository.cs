﻿using SalesForceReportSVC.DAL;
using SalesForceReportSVC.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalesForceReportSVC.Repositories
{
    public class WhatsAppUpdateContactsRepository: WhatsAppRepository<WhatsAppUpdateContactDto>, IWhatsAppUpdateContactsRepository
    {
        public WhatsAppUpdateContactsRepository(SalesForceReportLoaderContext context)
            :base(context)
        {

        }
    }
}
