﻿using SalesForceReportSVC.DAL;
using SalesForceReportSVC.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalesForceReportSVC.Repositories
{
    public class WhatsAppSTKPushRepository: WhatsAppRepository<WhatsAppSTKPushRequest>, IWhatsAppSTKPushRepository
    {

        public WhatsAppSTKPushRepository(SalesForceReportLoaderContext loaderContext)
            :base(loaderContext)
        {

        }
    }
}
