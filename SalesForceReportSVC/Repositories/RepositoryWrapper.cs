﻿using SalesForceReportSVC.DAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalesForceReportSVC.Repositories
{
    public class RepositoryWrapper: IRepositoryWrapper
    {
        private MPESAContext _MPESAContext;
        private SalesForceReportLoaderContext _loaderContext;

        private IMPESARepository mPESARepository;

        private IWhatsAppActClaimStatusRepository _whatsAppActClaim;

        private IWhatsAppPureClaimStatusRepository _whatsAppPureClaim;

        private IWhatsAppStatementRequestRepository _requestRepository;

        private IWhatsAppPurePolicyDetailsRepository _policyDetailsRepository;

        private ISalesForceDashboardReportRepository _reportRepository;
        //
        private IWhatsAppUpdateContactsRepository _whatsAppUpdate;
        private IWhatsAppSTKPushRepository _whatsAppSTK_Push;
        private ISalesForceDashboardReportRepository _salesForceDashboard;
        public RepositoryWrapper(MPESAContext mPESAContext, SalesForceReportLoaderContext reportLoaderContext)
        {
            this._loaderContext = reportLoaderContext;
            this._MPESAContext = mPESAContext;
        }

        public IMPESARepository MPESARepository
        {
            get
            {
                if(mPESARepository == null)
                {
                    mPESARepository = new MPESARepository(_MPESAContext);
                }
                return mPESARepository;
            }
        }

        public IWhatsAppActClaimStatusRepository whatsAppActClaim
        {
            get
            {
                if(_whatsAppActClaim == null)
                {
                    _whatsAppActClaim = new WhatsAppActClaimStatusRepository(_loaderContext);
                }
                return whatsAppActClaim;
            }
        }

        public IWhatsAppPureClaimStatusRepository whatsAppPureClaim
        {
            get
            {
                if(_whatsAppPureClaim == null)
                {
                    _whatsAppPureClaim = new WhatsAppPureClaimStatusRepository(_loaderContext);
                }
                return _whatsAppPureClaim;
            }
        }

        public IWhatsAppStatementRequestRepository statementRequestRepository
        {
            get
            {
                if(_requestRepository == null)
                {
                    _requestRepository = new WhatsAppStatementRequestRepository(_loaderContext);
                }
                return _requestRepository;
            }
        }

        public IWhatsAppPurePolicyDetailsRepository WhatsAppPurePolDetails
        {
            get
            {
                if(_policyDetailsRepository == null)
                {
                    _policyDetailsRepository = new WhatsAppPurePolicyDetailsRepository(_loaderContext);
                }
                return _policyDetailsRepository;
            }
        }

        public ISalesForceDashboardReportRepository reportRepository
        {
            get
            {
                if(_reportRepository == null)
                {
                    _reportRepository = new SalesForceDashboardReportRepository(_loaderContext);
                }
                return _reportRepository;
            }
        }


        public IWhatsAppUpdateContactsRepository whatsAppUpdateContacts
        {
            get
            {
                if(_whatsAppUpdate == null)
                {
                    _whatsAppUpdate = new WhatsAppUpdateContactsRepository(_loaderContext);

                }
                return _whatsAppUpdate;
            }
        }

        public IWhatsAppSTKPushRepository whatsAppSTK
        {
            get
            {
                if(_whatsAppSTK_Push == null)
                {
                    _whatsAppSTK_Push = new WhatsAppSTKPushRepository(_loaderContext);
                }
                return _whatsAppSTK_Push;
            }
        }

        public ISalesForceDashboardReportRepository salesforceLoader
        {
            get
            {
                if(_salesForceDashboard == null)
                {
                    _salesForceDashboard = new SalesForceDashboardReportRepository(_loaderContext);
                }
                return _salesForceDashboard;
            }
        }

        public void Save()
        {
            _loaderContext.SaveChanges();
        }
    }
}
