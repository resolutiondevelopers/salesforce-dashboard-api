﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesForceReportSVC.Helpers
{
    public static class MemoryStorageHelper
    {
        public static string Access_Token { get; set; }
        public static string Instance_Url { get; set; }
        public static string Token_Type { get; set; }
        public static string Signature { get; set; }


        public static Uri BaseEndpoint { get; set; }
        public static string ClientId { get; set; }
        public static string ClientSecret { get; set; }
        public static string Username { get; set; }
        public static string Password { get; set; }
    }
}
