﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SalesForceReportSVC.DTOs;
using SalesForceReportSVC.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesForceReportSVC.Controllers
{
    [Microsoft.AspNetCore.Mvc.Route("api/SalesForce")]
    [ApiController]
    public class SalesForceDataLoaderController: ControllerBase
    {
        private readonly IRepositoryWrapper repo_;
        private readonly ILogger logger_;
        private readonly IMapper mapper_;
        private readonly ISalesforceMedAPIClient _medAPIClient;
        public SalesForceDataLoaderController(ILogger<SalesForceDataLoaderController> logger, IRepositoryWrapper repo, IMapper mapper, ISalesforceMedAPIClient medAPIClient)
        {
            this.repo_ = repo;
            this.logger_ = logger;
            this.mapper_ = mapper;
            _medAPIClient = medAPIClient;
        }

        [Microsoft.AspNetCore.Mvc.HttpGet]
        [Microsoft.AspNetCore.Mvc.Route("DataLoader")]
        public ActionResult<IEnumerable<SalesForceProductionReportDto>> GetMedReport()
        {
            try
            {
                IQueryable<SalesForceProductionReportDto> dl = (this.repo_.salesforceLoader.FindAll());
                IEnumerable<SalesForceProductionReportDto> dLProductions = dl.Select(p => p).ToList();

                return Ok(dLProductions);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async ValueTask GetSDL()
        {
            // Get Data Loader List.
            try
            {
                //this.mapper_.Map < IEnumerable < DLProductionReportDto >>
                IQueryable<SalesForceProductionReportDto> dl = (this.repo_.salesforceLoader.FindAll());
                Console.WriteLine(dl.Count());
                IList<SalesForceProductionReportDto> dLProductions = await dl.Select(p => p).ToListAsync();

                //var options = new JsonSerializerOptions
                //{
                //    PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                //    WriteIndented = true
                //};

                //string json = JsonSerializer.Serialize<IEnumerable<DLProductionReportDto>>(dLProductions, options);
                await _medAPIClient.SalesforceLogin();
                bool res = await _medAPIClient.Post2MedSalesforce(dLProductions, "/data/v47.0/sobjects/Medical__c");

                if(res == true)
                {
                    this.logger_.LogInformation("Get Salesforce DataLoader OK - {time}", DateTimeOffset.Now);
                }
                else
                {
                    this.logger_.LogInformation("Get Salesforce DataLoader Failed - {time}", DateTimeOffset.Now);
                }
                
                
            }
            catch (Exception ex)
            {
                this.logger_.LogError($"Error accessing item {ex.InnerException} - " + "{time}", DateTimeOffset.Now);
                
            }
        }
    }
}
