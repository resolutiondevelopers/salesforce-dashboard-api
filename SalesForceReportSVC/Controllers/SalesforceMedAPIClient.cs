﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SalesForceReportSVC.DTOs;
using SalesForceReportSVC.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SalesForceReportSVC.Controllers
{
    /// <summary>
    /// Takes two generic items; T -Source of Data, U -Response Data
    /// </summary>
    /// <typeparam name="T"> Source of Data</typeparam>
    /// <typeparam name="U"> Response Data Type</typeparam>
    public class SalesforceMedAPIClient : ISalesforceMedAPIClient
    {
        private readonly HttpClient _httpClient;
        private IConfiguration Configuration;
        private ILogger logger_;
        private static string BaseEndpoint;
        private IList<string> SalesforceIds;

        public SalesforceMedAPIClient(IConfiguration configuration, ILogger logger)
        {
            this._httpClient = new HttpClient();
            Configuration = configuration;
            BaseEndpoint = Configuration.GetValue<string>("DefaultSetting:SalesforceEndpoint").ToString();
            logger_ = logger;
        }

        private HttpContent CreateSalesforceContent(SalesForceProductionReportDto content)
        {
            var json = JsonConvert.SerializeObject(content);
            return new StringContent(json, Encoding.UTF8, "application/json");
        }

        private Uri AddLoginHeaders(string uri)
        {
            UriBuilder builder = new UriBuilder(uri);

            var query = HttpUtility.ParseQueryString(builder.Query);
            query["grant_type"] = "password";
            query["client_id"] = MemoryStorageHelper.ClientId;
            query["client_secret"] = MemoryStorageHelper.ClientSecret;
            query["username"] = MemoryStorageHelper.Username;
            query["password"] = MemoryStorageHelper.Password;

            builder.Query = query.ToString();
            return builder.Uri;
        }

        private async void AddActiveHeader()
        {
            if (!String.IsNullOrEmpty(MemoryStorageHelper.Access_Token))
            {
                _httpClient.DefaultRequestHeaders.Add("Authorization",  $"Bearer {MemoryStorageHelper.Access_Token}");
                //_httpClient.DefaultRequestHeaders.Add("Content-Type", "application/json");
                _httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
            }
            else
            {
                await SalesforceLogin();
            }
        }

        public async ValueTask<bool> Post2MedSalesforce(IEnumerable<SalesForceProductionReportDto> salesForces, string endpoint)
        {
            this.SalesforceIds = new List<string>();
            if(salesForces != null)
            {
                try
                {
                    AddActiveHeader();
                    foreach (var salesForce in salesForces)
                    {
                        HttpContent content = CreateSalesforceContent(salesForce);
                        var response = await this._httpClient.PostAsync(MemoryStorageHelper.BaseEndpoint + endpoint, content);

                        var data = await response.Content.ReadAsStringAsync();

                        if (data.Contains("errorCode"))
                        {
                            List<SalesforceErrorResponseDto> data_err = JsonConvert.DeserializeObject<List<SalesforceErrorResponseDto>>(data);
                            this.logger_.LogError($"Error Posting to Salesforce {data_err[0].Message} - for JournalId --> {salesForce.Journal_ID__c} \n\n with {salesForce.Policy_Holder_ID__c.ToString()}\n\n" + "{time}", DateTimeOffset.Now);
                            continue;
                        }
                        else
                        {
                            SalesforceResponseDto data_res = JsonConvert.DeserializeObject<SalesforceResponseDto>(data);
                            this.SalesforceIds.Add(data_res.Id);
                        }
                        
                    }
                    
                    return true;
                }
                catch(Exception ex)
                {
                    this.logger_.LogError($"Error accessing item {ex.InnerException} - " + "{time}", DateTimeOffset.Now);
                    return false;
                }
                
            }
            else
            {
                return false;
            }
        }

        public async ValueTask SalesforceLogin()
        {
            StringContent content = new StringContent(String.Empty);
            MemoryStorageHelper.BaseEndpoint.ToString();
            Uri uri = AddLoginHeaders(BaseEndpoint + "/oauth2/token");


            var response_json = await _httpClient.PostAsync(uri, content);


            var data = await response_json.Content.ReadAsStringAsync();
            var response = JsonConvert.DeserializeObject<SalesforceLoginResponse>(data);

            bool isValid = Validator.TryValidateObject(response, new ValidationContext(response), new List<ValidationResult>());

            if (isValid)
            {
                MemoryStorageHelper.Access_Token = response.Access_Token;
                MemoryStorageHelper.Instance_Url = response.Instance_Url;
                MemoryStorageHelper.Token_Type = response.Token_Type;
                MemoryStorageHelper.Signature = response.Signature;
            }
            else
            {
                //log
            }
        }

        public async ValueTask LogIds(List<string> SalesforceIds)
        {
            // log

            if(SalesforceIds != null)
            {
                foreach(string id in SalesforceIds)
                {
                    //log
                }
            }
        }
    }
}
