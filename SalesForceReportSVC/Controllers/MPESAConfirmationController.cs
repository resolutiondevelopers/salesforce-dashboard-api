﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SalesForceReportSVC.DTOs;
using SalesForceReportSVC.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SalesForceReportSVC.Controllers
{
    public class ConfirmationResult
    {
        public string Status { get; set; }
        public string TransID { get; set; }
    }

    [Microsoft.AspNetCore.Mvc.Route("api/MPESA")]
    [ApiController]
    public class MPESAConfirmationController: ControllerBase
    {
        private readonly IRepositoryWrapper repo_;
        private readonly ILogger logger_;

        public MPESAConfirmationController(IRepositoryWrapper wrapper, ILogger<MPESAConfirmationDto> logger)
        {
            this.repo_ = wrapper;
            this.logger_ = logger;
        }

        [Microsoft.AspNetCore.Mvc.HttpGet]
        [Microsoft.AspNetCore.Mvc.Route("Confirmation/{regno}")]
        public ActionResult<ConfirmationResult> GetMPESAConfirmation(string regno)
        {
            ConfirmationResult confirmation = null;
            try
            {
                
                IQueryable<MPESAConfirmationDto> confirmationDtos = this.repo_.MPESARepository.FindMPESAByCondition(x => x.BillRefNumber == regno);
                MPESAConfirmationDto confirmationDto = confirmationDtos.Where(x => x.Timestamp >= DateTime.Now.AddDays(-1)).SingleOrDefault();
                
                if (confirmationDto != null)
                {
                    confirmation = new ConfirmationResult
                    {
                        Status = "Confirmed Payment",
                        TransID = confirmationDto.TransID
                    };

                    return confirmation;
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine($"Err -------{ex.Message}");
            }
            //IQueryable<MPESAConfirmationDto> confirmationDtos = this.repo_.MPESARepository.FindMPESAByCondition(x => x.BillRefNumber == regno);
            
            

            confirmation = new ConfirmationResult
            {
                Status = "Failed Confirmation",
                TransID = ""
            };
            return confirmation;
        }
    }
}
