﻿using AutoMapper;
using AutoMapper.Configuration;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using SalesForceReportSVC.DTOs;
using SalesForceReportSVC.Repositories;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SalesForceReportSVC.Controllers
{
    [Microsoft.AspNetCore.Mvc.Route("api/WhatsApp")]
    [ApiController]
    public class WhatsAppController: ControllerBase
    {
        private const string STKPushUri = "http://192.168.0.102/api/v1/ril/stk/push?";
        private const string REFID = "RefId";
        private readonly IRepositoryWrapper wrapper_;
        private readonly ILogger logger_;
        private readonly IMapper mapper_;


        public WhatsAppController(IRepositoryWrapper wrapper, ILogger<WhatsAppController> logger, IMapper mapper)
        {
            this.wrapper_ = wrapper;
            this.logger_ = logger;
            this.mapper_ = mapper;

        }


        [HttpPost]
        [Microsoft.AspNetCore.Mvc.Route("AddSTKRequest")]
        public async Task<ActionResult<WhatsAppResponseDto<string>>> AddSTKRequest(WhatsAppSTKPushRequest pushRequest)
        {
            StringValues RefId = String.Empty;

            Request.Headers.TryGetValue(REFID, out RefId);


            try
            {
                if (!String.IsNullOrEmpty(RefId))
                {
                    bool stkres = await STKPush(pushRequest);

                    if (stkres)
                    {
                        pushRequest.Picked = 1;
                        pushRequest.ReqTime = DateTime.Now;
                        this.wrapper_.whatsAppSTK.Create(pushRequest);

                        WhatsAppResponseDto<string> responseDto = new WhatsAppResponseDto<string>
                        {
                            RefId = RefId,
                            CallBackResponse = "Your payment request has been received!",
                            Errors = new List<Errors>()
                        };
                        this.wrapper_.Save();
                        return Ok(responseDto);
                    }
                    else
                    {
                        pushRequest.Picked = 2;
                        pushRequest.ReqTime = DateTime.Now;
                        this.wrapper_.whatsAppSTK.Create(pushRequest);

                        WhatsAppResponseDto<string> responseDto = new WhatsAppResponseDto<string>
                        {
                            RefId = RefId,
                            CallBackResponse = "An Error occured during your payment request!",
                            Errors = new List<Errors>()
                        };
                        this.wrapper_.Save();
                        return BadRequest(responseDto);
                    }

                }
                else
                {
                    WhatsAppResponseDto<string> responseDto = new WhatsAppResponseDto<string>
                    {
                        RefId = RefId,
                        CallBackResponse = null,
                        Errors = new List<Errors>()
                        {
                            new Errors{ Error = "Ref Id Not Found"}
                        }
                    };
                    return BadRequest(responseDto);
                }

            }
            catch (Exception ex)
            {
                WhatsAppResponseDto<string> responseDto = new WhatsAppResponseDto<string>
                {
                    RefId = RefId,
                    CallBackResponse = null,
                    Errors = new List<Errors>()
                        {
                            new Errors{Error = "Internal Server Error: " + ex.Message}
                        }

                };
                return BadRequest(responseDto);
            }
        }


        public async Task<Boolean> STKPush(WhatsAppSTKPushRequest pushRequest)
        {
            try
            {
                HttpClient client = new HttpClient();
                STKPushReqDto pushReqDto = this.mapper_.Map<STKPushReqDto>(pushRequest);

                string STK_params = $"AccountNumber={pushReqDto.AccountNumber}&MSISDN={pushReqDto.MSISDN}&Amount={pushReqDto.Amount}&Description={pushReqDto.Description}";

                HttpResponseMessage httpResponse = await client.PostAsync(STKPushUri + STK_params, null);

                if (httpResponse.IsSuccessStatusCode)
                {
                    //something
                    return true;
                }
                else
                {
                    //log it
                    return false;
                }
            }
            catch (Exception ex)
            {
                // Exception
                return false;
            }

        }

        [HttpPost]
        [Microsoft.AspNetCore.Mvc.Route("UpdateContact")]
        public async Task<ActionResult<WhatsAppResponseDto<string>>> UpdateContacts(WhatsAppUpdateContactDto updateContactRequest)
        {
            StringValues RefId = String.Empty;

            Request.Headers.TryGetValue(REFID, out RefId);

            try
            {
                if (!String.IsNullOrEmpty(RefId))
                {

                    this.wrapper_.whatsAppUpdateContacts.Create(updateContactRequest);

                    WhatsAppResponseDto<string> responseDto = new WhatsAppResponseDto<string>
                    {
                        RefId = RefId,
                        CallBackResponse = "Your contacts have been updated!",
                        Errors = new List<Errors>()
                    };
                    this.wrapper_.Save();
                    return Ok(responseDto);

                }
                else
                {
                    WhatsAppResponseDto<string> responseDto = new WhatsAppResponseDto<string>
                    {
                        RefId = RefId,
                        CallBackResponse = null,
                        Errors = new List<Errors>()
                        {
                            new Errors{ Error = "Ref Id Not Found"}
                        }
                    };
                    return BadRequest(responseDto);
                }
            }
            catch (Exception ex)
            {
                WhatsAppResponseDto<string> responseDto = new WhatsAppResponseDto<string>
                {
                    RefId = RefId,
                    CallBackResponse = null,
                    Errors = new List<Errors>()
                        {
                            new Errors{Error = "Internal Server Error: " + ex.Message}
                        }

                };
                return BadRequest(responseDto);
            }
        }
    }
}
