﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using SalesForceReportSVC.DTOs;
using SalesForceReportSVC.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace SalesForceReportSVC.Controllers
{
    [Route("api/WhatsApp")]
    [ApiController]
    public class PureWhatsAppController : ControllerBase
    {
        private const string REFID = "RefId";
        private readonly IRepositoryWrapper wrapper_;
        private readonly ILogger logger_;
        private readonly IMapper mapper_;

        public PureWhatsAppController(IRepositoryWrapper wrapper, ILogger<PureWhatsAppController> logger, IMapper mapper)
        {
            this.wrapper_ = wrapper;
            this.logger_ = logger;
            this.mapper_ = mapper;
        }

        public string RegistrationNumberSplitter(string regno)
        {
            string regno_a = Regex.Split(regno.Trim(), @"([a-zA-Z]){3,4}", RegexOptions.IgnorePatternWhitespace | RegexOptions.Compiled).LastOrDefault();
            string regno_b = Regex.Split(regno.Trim(), @"([0-9]){3}([a-zA-Z]){1}", RegexOptions.IgnorePatternWhitespace).FirstOrDefault();

            regno_a = regno_a.Replace(" ", string.Empty);
            regno_b = regno_b.Replace(" ", string.Empty);
            return $"{regno_b} {regno_a}";//.Replace(" ", string.Empty);
        }

        [Microsoft.AspNetCore.Mvc.HttpGet]
        [Microsoft.AspNetCore.Mvc.Route("GBClaimStatus/{regNo}")]
        public ActionResult<WhatsAppResponseDto<PureWhatsAppClaimStatusDto>> GetPureClaimStatus(string regNo)
        {
            string regno = RegistrationNumberSplitter(regNo);
            StringValues RefId = String.Empty;

            //string RefId = String.Empty;
            Request.Headers.TryGetValue(REFID, out RefId);

            try
            {

                if (!String.IsNullOrEmpty(RefId) && !String.IsNullOrEmpty(regNo))
                {
                    IQueryable<PureWhatsAppClaimStatusDto> claimStatusDtos = this.wrapper_.whatsAppPureClaim.FindByCondition(x => x.RegNo == regno);
                    PureWhatsAppClaimStatusDto pureWhatsApp = claimStatusDtos.SingleOrDefault();//.FirstOrDefault();

                    if (pureWhatsApp != null)
                    {
                        WhatsAppResponseDto<PureWhatsAppClaimStatusDto> responseDto = new WhatsAppResponseDto<PureWhatsAppClaimStatusDto>
                        {
                            RefId = RefId,
                            CallBackResponse = pureWhatsApp,
                            Errors = new List<Errors>()
                        };

                        return Ok(responseDto);
                    }
                    else
                    {
                        WhatsAppResponseDto<PureWhatsAppClaimStatusDto> responseDto = new WhatsAppResponseDto<PureWhatsAppClaimStatusDto>
                        {
                            RefId = RefId,
                            CallBackResponse = null,
                            Errors = new List<Errors>()
                        {
                            new Errors{Error = "Vehicle Registration Number Not Found"}
                        }

                        };

                        return NotFound(responseDto);
                    }
                }
                else
                {
                    WhatsAppResponseDto<PureWhatsAppClaimStatusDto> responseDto = new WhatsAppResponseDto<PureWhatsAppClaimStatusDto>
                    {
                        RefId = RefId,
                        CallBackResponse = null,
                        Errors = new List<Errors>()
                        {
                            new Errors{Error = "Registration Number/RefId Is missing"}
                        }

                    };

                    return BadRequest(responseDto);
                }
            }
            catch (Exception ex)
            {
                WhatsAppResponseDto<PureWhatsAppClaimStatusDto> responseDto = new WhatsAppResponseDto<PureWhatsAppClaimStatusDto>
                {
                    RefId = RefId,
                    CallBackResponse = null,
                    Errors = new List<Errors>()
                        {
                            new Errors{Error = "Internal Server Error"}
                        }

                };
                return BadRequest(responseDto);
            }


        }

        [Microsoft.AspNetCore.Mvc.HttpGet]
        [Microsoft.AspNetCore.Mvc.Route("GBPolicyDetails/{regNo}")]
        public ActionResult<WhatsAppResponseDto<PureWhatsAppPolicyDetailsDto>> GetPurePolDetails(string regNo)
        {
            string regno = RegistrationNumberSplitter(regNo);
            StringValues RefId = String.Empty;
            //string RefId = String.Empty;
            Request.Headers.TryGetValue(REFID, out RefId);

            try
            {

                if (!String.IsNullOrEmpty(RefId) && !String.IsNullOrEmpty(regNo))
                {
                    IQueryable<PureWhatsAppPolicyDetailsDto> claimStatusDtos = this.wrapper_.WhatsAppPurePolDetails.FindByCondition(x => x.RegNo == regno);
                    PureWhatsAppPolicyDetailsDto pureWhatsApp = claimStatusDtos.SingleOrDefault();//.FirstOrDefault();

                    if (pureWhatsApp != null)
                    {
                        WhatsAppResponseDto<PureWhatsAppPolicyDetailsDto> responseDto = new WhatsAppResponseDto<PureWhatsAppPolicyDetailsDto>
                        {
                            RefId = RefId,
                            CallBackResponse = pureWhatsApp,
                            Errors = new List<Errors>()
                        };

                        return Ok(responseDto);
                    }
                    else
                    {
                        WhatsAppResponseDto<PureWhatsAppPolicyDetailsDto> responseDto = new WhatsAppResponseDto<PureWhatsAppPolicyDetailsDto>
                        {
                            RefId = RefId,
                            CallBackResponse = null,
                            Errors = new List<Errors>()
                        {
                            new Errors{Error = "Vehicle Registration Number Not Found"}
                        }

                        };

                        return NotFound(responseDto);
                    }
                }
                else
                {
                    WhatsAppResponseDto<PureWhatsAppPolicyDetailsDto> responseDto = new WhatsAppResponseDto<PureWhatsAppPolicyDetailsDto>
                    {
                        RefId = RefId,
                        CallBackResponse = null,
                        Errors = new List<Errors>()
                        {
                            new Errors{Error = "Registration Number/RefId Is missing"}
                        }

                    };

                    return BadRequest(responseDto);
                }
            }
            catch (Exception ex)
            {
                WhatsAppResponseDto<PureWhatsAppPolicyDetailsDto> responseDto = new WhatsAppResponseDto<PureWhatsAppPolicyDetailsDto>
                {
                    RefId = RefId,
                    CallBackResponse = null,
                    Errors = new List<Errors>()
                        {
                            new Errors{Error = "Internal Server Error"}
                        }

                };
                return BadRequest(responseDto);
            }
        }

        [Microsoft.AspNetCore.Mvc.HttpPost]
        [Microsoft.AspNetCore.Mvc.Route("GBAccStatementReq")]
        public ActionResult<WhatsAppResponseDto<string>> PostAccStatementReq([FromBody]PureWhatsAppStatementReq statementReq)
        {
            if (ModelState.IsValid)
            {
                StringValues RefId = String.Empty;
                //string RefId = String.Empty;
                Request.Headers.TryGetValue(REFID, out RefId);

                try
                {
                    if (!String.IsNullOrEmpty(RefId))
                    {
                        WhatsAppStatementRequestDto statementDto = this.mapper_.Map<WhatsAppStatementRequestDto>(statementReq);
                        statementDto.SType = 1;
                        statementDto.RefNumber = RefId;

                        this.wrapper_.statementRequestRepository.Create(statementDto);
                        this.wrapper_.Save();
                        WhatsAppResponseDto<string> responseDto = new WhatsAppResponseDto<string>
                        {
                            RefId = RefId,
                            CallBackResponse = "Account Statement Request has been received",
                            Errors = new List<Errors>()
                        };

                        return Ok(responseDto);
                    }
                    else
                    {
                        WhatsAppResponseDto<string> responseDto = new WhatsAppResponseDto<string>
                        {
                            RefId = RefId,
                            CallBackResponse = null,
                            Errors = new List<Errors>()
                        {
                            new Errors{ Error = "Ref Id Not Found"}
                        }
                        };
                        return BadRequest(responseDto);
                    }

                }
                catch (Exception ex)
                {
                    WhatsAppResponseDto<string> responseDto = new WhatsAppResponseDto<string>
                    {
                        RefId = RefId,
                        CallBackResponse = null,
                        Errors = new List<Errors>()
                        {
                            new Errors{Error = "Internal Server Error"}
                        }

                    };
                    return BadRequest(responseDto);
                }
            }
            else
            {
                WhatsAppResponseDto<string> responseDto = new WhatsAppResponseDto<string>
                {
                    RefId = null,
                    CallBackResponse = null,
                    Errors = new List<Errors>()
                        {
                            new Errors{Error = "Model Is invalid Error"}
                        }

                };
                return BadRequest(responseDto);
            }
        }


        [Microsoft.AspNetCore.Mvc.HttpPost]
        [Microsoft.AspNetCore.Mvc.Route("GBCSStatementReq")]
        public ActionResult<WhatsAppResponseDto<string>> PostClaimStatusStatementReq([FromBody]PureWhatsAppStatementReq statementReq)
        {
            if (ModelState.IsValid)
            {
                StringValues RefId = String.Empty;
                //string RefId = String.Empty;
                Request.Headers.TryGetValue(REFID, out RefId);

                try
                {
                    if (!String.IsNullOrEmpty(RefId))
                    {
                        WhatsAppStatementRequestDto statementDto = this.mapper_.Map<WhatsAppStatementRequestDto>(statementReq);
                        statementDto.SType = 2;
                        statementDto.RefNumber = RefId;

                        this.wrapper_.statementRequestRepository.Create(statementDto);
                        this.wrapper_.Save();
                        WhatsAppResponseDto<String> responseDto = new WhatsAppResponseDto<String>
                        {
                            RefId = RefId,
                            CallBackResponse = "Claim Statement Utilization Request has been received",
                            Errors = new List<Errors>()
                        };

                        return Ok(responseDto);
                    }
                    else
                    {
                        WhatsAppResponseDto<string> responseDto = new WhatsAppResponseDto<string>
                        {
                            RefId = RefId,
                            CallBackResponse = null,
                            Errors = new List<Errors>()
                        {
                            new Errors{ Error = "RefId Not Found"}
                        }
                        };
                        return BadRequest(responseDto);
                    }

                }
                catch (Exception ex)
                {
                    WhatsAppResponseDto<string> responseDto = new WhatsAppResponseDto<string>
                    {
                        RefId = RefId,
                        CallBackResponse = null,
                        Errors = new List<Errors>()
                        {
                            new Errors{Error = "Internal Server Error"}
                        }

                    };
                    return BadRequest(responseDto);
                }
            }
            else
            {
                WhatsAppResponseDto<string> responseDto = new WhatsAppResponseDto<string>
                {
                    RefId = null,
                    CallBackResponse = null,
                    Errors = new List<Errors>()
                        {
                            new Errors{Error = "Model Is invalid Error"}
                        }

                };
                return BadRequest(responseDto);
            }
        }

    }
}
