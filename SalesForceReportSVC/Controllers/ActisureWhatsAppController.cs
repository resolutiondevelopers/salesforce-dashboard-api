﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using SalesForceReportSVC.DTOs;
using SalesForceReportSVC.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SalesForceReportSVC.Controllers
{

    [Microsoft.AspNetCore.Mvc.Route("api/WhatsApp")]
    [ApiController]
    public class ActisureWhatsAppController: ControllerBase
    {
        private const string REFID = "RefId";
        private readonly IRepositoryWrapper wrapper_;
        private readonly ILogger logger_;
        private readonly IMapper mapper_;

        public ActisureWhatsAppController(ILogger<ActisureWhatsAppController> logger, IRepositoryWrapper wrapper, IMapper mapper)
        {
            this.wrapper_ = wrapper;
            this.logger_ = logger;
            this.mapper_ = mapper;
        }


        [Microsoft.AspNetCore.Mvc.HttpGet]
        [Microsoft.AspNetCore.Mvc.Route("ActClaimStatus/{recptid}")]
        public ActionResult<WhatsAppResponseDto<ActWhatsAppClaimStatusDto>> GetClaimStatus(string recptid)
        {
            StringValues RefId = String.Empty;
            //string RefId = String.Empty;
            Request.Headers.TryGetValue(REFID, out RefId);
            try
            {

                if (!String.IsNullOrEmpty(RefId.ToString()))
                {
                    //Query 
                    IQueryable<ActWhatsAppClaimStatusDto> claimStatusDtos = this.wrapper_.whatsAppActClaim.FindByCondition(x => x.InvoiceReference == recptid);
                    ActWhatsAppClaimStatusDto actWhatsApp = claimStatusDtos.SingleOrDefault();

                    if (actWhatsApp != null)
                    {
                        WhatsAppResponseDto<ActWhatsAppClaimStatusDto> responseDto = new WhatsAppResponseDto<ActWhatsAppClaimStatusDto>
                        {
                            RefId = RefId,
                            CallBackResponse = actWhatsApp,
                            Errors = new List<Errors>()
                        };

                        return Ok(responseDto);
                    }
                    else
                    {
                        WhatsAppResponseDto<ActWhatsAppClaimStatusDto> responseDto = new WhatsAppResponseDto<ActWhatsAppClaimStatusDto>
                        {
                            RefId = RefId,
                            CallBackResponse = null,
                            Errors = new List<Errors>
                            {
                                new Errors{ Error = "Claim Status Not Found"}
                            }
                        };

                        return NotFound(responseDto);
                    }


                }
                else
                {
                    WhatsAppResponseDto<ActWhatsAppClaimStatusDto> responseDto = new WhatsAppResponseDto<ActWhatsAppClaimStatusDto>
                    {
                        RefId = RefId,
                        CallBackResponse = null,
                        Errors = new List<Errors>
                            {
                                new Errors{ Error = "RefId is Missing"}
                            }
                    };
                    return BadRequest(responseDto);
                }

            }
            catch (Exception ex)
            {
                this.logger_.LogError($"Error accessing item {ex.InnerException} - " + "{time}", DateTimeOffset.Now);
                WhatsAppResponseDto<ActWhatsAppClaimStatusDto> responseDto = new WhatsAppResponseDto<ActWhatsAppClaimStatusDto>
                {
                    RefId = RefId,
                    CallBackResponse = null,
                    Errors = new List<Errors>
                            {
                                new Errors{ Error = "Internal Server Error"}
                            }
                };
                return BadRequest(responseDto);
            }
        }

        [Microsoft.AspNetCore.Mvc.HttpPost]
        [Microsoft.AspNetCore.Mvc.Route("ActAccStatementReq")]
        public ActionResult<WhatsAppResponseDto<string>> PostAccStatementReq([FromBody]ActWhatsAppStatementReq statementReq)
        {
            if (ModelState.IsValid)
            {
                StringValues RefId = String.Empty;
                //string RefId = String.Empty;
                Request.Headers.TryGetValue(REFID, out RefId);

                try
                {
                    if (!String.IsNullOrEmpty(RefId))
                    {
                        WhatsAppStatementRequestDto statementDto = this.mapper_.Map<WhatsAppStatementRequestDto>(statementReq);
                        statementDto.SType = 1;
                        statementDto.RefNumber = RefId;

                        this.wrapper_.statementRequestRepository.Create(statementDto);
                        this.wrapper_.Save();
                        WhatsAppResponseDto<String> responseDto = new WhatsAppResponseDto<String>
                        {
                            RefId = RefId,
                            CallBackResponse = "Account Statement Request has been received",
                            Errors = new List<Errors>()
                        };

                        return Ok(responseDto);
                    }
                    else
                    {
                        WhatsAppResponseDto<String> responseDto = new WhatsAppResponseDto<String>
                        {
                            RefId = RefId,
                            CallBackResponse = null,
                            Errors = new List<Errors>()
                        {
                            new Errors{ Error = "Ref Id Not Found"}
                        }
                        };
                        return BadRequest(responseDto);
                    }

                }
                catch (Exception ex)
                {
                    WhatsAppResponseDto<String> responseDto = new WhatsAppResponseDto<String>
                    {
                        RefId = RefId,
                        CallBackResponse = null,
                        Errors = new List<Errors>()
                        {
                            new Errors{Error = "Internal Server Error"}
                        }

                    };
                    return BadRequest(responseDto);
                }
            }
            else
            {
                WhatsAppResponseDto<String> responseDto = new WhatsAppResponseDto<String>
                {
                    RefId = null,
                    CallBackResponse = null,
                    Errors = new List<Errors>()
                        {
                            new Errors{Error = "Model Is invalid Error"}
                        }

                };
                return BadRequest(responseDto);
            }
        }


        [Microsoft.AspNetCore.Mvc.HttpPost]
        [Microsoft.AspNetCore.Mvc.Route("ActCSStatementReq")]
        public ActionResult<WhatsAppResponseDto<string>> PostClaimStatusStatementReq([FromBody]ActWhatsAppStatementReq statementReq)
        {
            if (ModelState.IsValid)
            {
                StringValues RefId = String.Empty;
                //string RefId = String.Empty;
                Request.Headers.TryGetValue(REFID, out RefId);

                try
                {
                    if (!String.IsNullOrEmpty(RefId))
                    {
                        WhatsAppStatementRequestDto statementDto = this.mapper_.Map<WhatsAppStatementRequestDto>(statementReq);
                        statementDto.SType = 2;
                        statementDto.RefNumber = RefId;

                        this.wrapper_.statementRequestRepository.Create(statementDto);
                        this.wrapper_.Save();
                        WhatsAppResponseDto<String> responseDto = new WhatsAppResponseDto<String>
                        {
                            RefId = RefId,
                            CallBackResponse = "Claim Statement Utilization Request has been received",
                            Errors = new List<Errors>()
                        };

                        return Ok(responseDto);
                    }
                    else
                    {
                        WhatsAppResponseDto<String> responseDto = new WhatsAppResponseDto<String>
                        {
                            RefId = RefId,
                            CallBackResponse = null,
                            Errors = new List<Errors>()
                        {
                            new Errors{ Error = "Ref Id Not Found"}
                        }
                        };
                        return BadRequest(responseDto);
                    }

                }
                catch (Exception ex)
                {
                    WhatsAppResponseDto<String> responseDto = new WhatsAppResponseDto<String>
                    {
                        RefId = RefId,
                        CallBackResponse = null,
                        Errors = new List<Errors>()
                        {
                            new Errors{Error = "Internal Server Error"}
                        }

                    };
                    return BadRequest(responseDto);
                }
            }
            else
            {
                WhatsAppResponseDto<String> responseDto = new WhatsAppResponseDto<String>
                {
                    RefId = null,
                    CallBackResponse = null,
                    Errors = new List<Errors>()
                        {
                            new Errors{Error = "Model Is invalid Error"}
                        }

                };
                return BadRequest(responseDto);
            }
        }
    }
}
