﻿using SalesForceReportSVC.DTOs;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SalesForceReportSVC.Controllers
{
    public interface ISalesforceMedAPIClient
    { 
        ValueTask<bool> Post2MedSalesforce(IEnumerable<SalesForceProductionReportDto> salesForce, string endpoint);
        ValueTask SalesforceLogin();
    }
}
