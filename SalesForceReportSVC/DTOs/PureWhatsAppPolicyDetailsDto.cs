﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesForceReportSVC.DTOs
{
    public class PureWhatsAppPolicyDetailsDto
    {
        public string PolicyNumber { get; set; }
        public DateTime CoverStartDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string Certificate_No { get; set; }
        public string RegNo { get; set; }
    }
}
