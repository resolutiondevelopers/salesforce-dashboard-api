﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SalesForceReportSVC.DTOs
{
    public class WhatsAppSTKPushRequest
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int32 Id { get; set; }
        [Required]
        public string AccountNumber { get; set; }
        [Required]
        public string MSISDN { get; set; }
        [Required]
        public string PaymentFor { get; set; }
        [Required]
        public Decimal Amount { get; set; }
        public Int32 Picked { get; set; }
        public DateTime ReqTime { get; set; }
    }
}
