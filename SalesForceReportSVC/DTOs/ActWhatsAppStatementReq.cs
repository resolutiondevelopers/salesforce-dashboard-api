﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SalesForceReportSVC.DTOs
{
    public class ActWhatsAppStatementReq
    {
        [Required]
        public string EmailAddress { get; set; }
        [Required]
        public string MemberNumber { get; set; }
        [Required]
        public string MSISDN { get; set; }
        [Required]
        public string SPeriod { get; set; }
    }
}
