﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesForceReportSVC.DTOs
{
    public class ActWhatsAppClaimStatusDto
    {
        public Int32 AssessmentId { get; set; }
        public Int32 BeneficiaryId { get; set; }
        public string BeneficiaryName { get; set; }
        public Decimal InvoiceAmount { get; set; }
        public string InvoiceReference { get; set; }
        public DateTime InvoiceDate { get; set; }
        public DateTime TreatmentDate { get; set; }
        public string InvoiceStatus { get; set; }
    }
}
