﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesForceReportSVC.DTOs
{
    public class SalesforceErrorResponseDto
    {
        public string Message { get; set; }
        public string ErrorCode { get; set; }

        public List<string> Fields { get; set; }
    }

}
