﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesForceReportSVC.DTOs
{
    public class SalesForceProductionReportDto
    {
        public Int32? Journal_ID__c { get; set; }
        public string Journal_Description__c { get; set; }
        public DateTime? Policy_Start_Date__c { get; set; }
        public DateTime? Policy_End_Date__c { get; set; }
        public DateTime? Livening_Date__c { get; set; }
        public DateTime? Effective_Date__c { get; set; }
        public DateTime? Renewal_Date__c { get; set; }
        public string Entity_Name__c { get; set; }
        public string Policy_Holder_ID__c { get; set; }
        public string Name { get; set; }
        public Decimal? Debit__c { get; set; }
        public Decimal? Credit__c { get; set; }
        public Decimal? Balance__c { get; set; }
        public string Account_Manager__c { get; set; }
        //public string Agent { get; set; }
        public string Type_of_Business__c { get; set; }
        public string Policy_Status__c { get; set; }
        public string Region__c { get; set; }
        //public Int32? DateDifference { get; set; }
    }
}
