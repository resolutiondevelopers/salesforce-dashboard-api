﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SalesForceReportSVC.DTOs
{
    public class SalesforceLoginResponse
    {
        [Required]
        public string Access_Token { get; set; }
        [Required]
        public string Instance_Url { get; set; }
        public string Id { get; set; }
        [Required]
        public string Token_Type { get; set; }
        public DateTime IssuedAt { get; set; }
        [Required]
        public string Signature { get; set; }
    }
}
