﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesForceReportSVC.DTOs
{
    public class MPESAConfirmationDto
    {
        public Int32 _id { get; set; }
        public string BillRefNumber { get; set; }
        public string TransID { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
