﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesForceReportSVC.DTOs
{
    public class STKPushReqDto
    {
        public string AccountNumber { get; set; }
        public string MSISDN { get; set; }
        public Int32 Amount { get; set; }
        public string Description { get; set; }
    }
}
