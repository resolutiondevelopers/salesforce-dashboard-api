﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesForceReportSVC.DTOs
{
    public class PureWhatsAppClaimStatusDto
    {
        public string ClaimNo { get; set; }
        public Decimal SumInsured { get; set; }
        public string ClientName { get; set; }
        public DateTime LossDate { get; set; }
        public string RegNo { get; set; }
        public string Status { get; set; }
        public Decimal IncurredAmount { get; set; }
    }
}
