﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace SalesForceReportSVC.DTOs
{
    public class WhatsAppResponseDto<T> where T : class
    {
        [JsonPropertyName("RefId")]
        public string RefId { get; set; }
        [JsonPropertyName("CallBackResponse")]
        public T CallBackResponse { get; set; }
        [JsonPropertyName("Errors")]
        public List<Errors> Errors { get; set; }
    }

    public class Errors
    {
        public string Error { get; set; }
    }
}
