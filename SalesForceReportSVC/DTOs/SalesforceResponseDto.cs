﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesForceReportSVC.DTOs
{
    public class SalesforceResponseDto
    {
        public string Id { get; set; }
        public bool Success { get; set; }
        public List<string> Errors { get; set; }
    }
}
