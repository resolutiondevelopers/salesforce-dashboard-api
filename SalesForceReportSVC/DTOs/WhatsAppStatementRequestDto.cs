﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SalesForceReportSVC.DTOs
{
    public class WhatsAppStatementRequestDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int32 StId { get; set; }
        public Int16 BArea { get; set; }
        public Int16 SType { get; set; }
        public string RefNumber { get; set; }
        public string EmailAddress { get; set; }
        public string MSISDN { get; set; }
        public Int16 Speriod { get; set; }
        public string MemberNumber { get; set; }
        public string RegNumber { get; set; }
        public DateTime dateRequested { get; set; }
        public string status { get; set; }
    }
}
